import {SelectItem} from 'primeng/primeng';

//DDLB für Filter der Bookliste
//Dies ist das Model für filter.html
//DDLB ist p-dropdown von Prime Ng
export class FilterModel {
  private _bookTypeSelectionList:SelectItem[];// [options]="model.bookTypeSelectionList" 
  private _selectedBookType:string;//Der ausgewählt Wert der DDLB // [(ngModel)]="model._selectedBookType"
/*<filter.html
  <p-dropdown [options]="model.bookTypeSelectionList" 
    [(ngModel)]="model._selectedBookType" [style]="{'width':'120px'}" 
    (onChange)="typeSelected()">//filter.component.ts: typeSelected
  </p-dropdown>
*/
  constructor() {
    this._bookTypeSelectionList = [];//Aufbau der DDLB-Werte für das Prime-Ng Dropdown
    this._bookTypeSelectionList.push({label: 'Alle Autoren', value: 'ALL'});
    this._bookTypeSelectionList.push({label: 'Jules Verne', value: 'V'});
    this._bookTypeSelectionList.push({label: 'Thomas Mann', value: 'M'});
    this._bookTypeSelectionList.push({label: 'Edgar Brau', value: 'B'});
    this._bookTypeSelectionList.push({label: 'Jack London', value: 'L'});
    this._selectedBookType = 'Alle Autoren anzeigen';
  }
   //booklist.html: <filter [model]="model" (doFilter)="getFilteredSelectionList()">
  //Methode ist mit der DDLB und filter.model.ts verbunden
  //Ruft Service-Methode auf, BookType Vegetarisch etc.
  
  get bookTypeSelectionList():Array<SelectItem> {
    return this._bookTypeSelectionList;
  }

  set bookTypeSelectionList(value:Array<SelectItem>) {
    this._bookTypeSelectionList = value;
  }

  get selectedBookType():string {//Auswahl wird in booklist.component.ts aufgerufen.
    return this._selectedBookType;
  }

  set selectedBookType(value:string) {
    this._selectedBookType = value;
  }
}

