import {Component, Input, Output, EventEmitter} from '@angular/core';
import {FilterModel} from '../models/filter.model';
//ist in booklist.html eingebaut, selector:filter
@Component({
  selector: 'filter',
  styleUrls: ['app/css/styles.css'],
  templateUrl: 'app/filter/templates/filter.html'
})//sele3ctor: filter: in booklist.html eingebaut
export class FilterComponent {
  @Input() model:FilterModel;
  @Output() doFilter = new EventEmitter(); //<filter [model]="model" (doFilter)="getFilteredSelectionList
  //EventEmitter: Event (doFilter) von booklist.html, booklist ist der Parent von filter.html
  constructor() {}
   //booklist.html: <filter [model]="model" (doFilter)="getFilteredSelectionList()">
  //Methode ist mit der DDLB und filter.model.ts verbunden
  //Ruft Service-Methode auf, BookType Vegetarisch etc.
  getFilteredSelectionList() {
    this.doFilter.emit({});
  }

  typeSelected() {//filter.html: (onChange)="typeSelected()">
    this.getFilteredSelectionList();
  }
}


