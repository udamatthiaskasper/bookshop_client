import {Component} from '@angular/core';
import {MenuItem} from 'primeng/primeng';
import {Router} from "@angular/router";
import {AuthService} from './login/ts/services/auth.service';

@Component({
  selector: 'bookshop',
  templateUrl: 'app/app.html',
  styleUrls: ['app/css/styles.css'],
})
export class AppComponent {
  private items: MenuItem[];

  constructor(private _router:Router, 
              private _authService:AuthService){
    localStorage.removeItem('auth_token');
    this.items = [
      {label: 'Bücherliste', icon: 'fa fa-book', routerLink: ['']},
      {label: 'Warenkorb', icon: '', routerLink: ['/cart']},
      {label: 'Verlauf', icon: '', routerLink: ['/orderhistory']}
      ]
  }

  logout() {
    this._authService.logout();
  }

  login() {
    this._router.navigate(['login']);
  }

  isLoggedIn() {
    return this._authService.isLoggedIn();
  }
}