import {Routes, RouterModule} from '@angular/router';//Import Angular Router-Module
import {BookRoutes} from './book/ts/routes/book.routes';
import {CartRoutes} from './cart/ts/routes/cart.routes';
import {CheckoutRoutes} from './checkout/ts/routes/checkout.routes';
import {OrderOptionRoutes} from './order-option/ts/routes/order-option.routes';
import {LoginRoutes} from './login/ts/routes/login.routes';
import {ConfirmationRoutes} from './confirmation/ts/routes/confirmation.routes';
import {AccountCreatorRoutes} from './accountcreator/ts/routes/account-creator.routes';
import {OrderHistoryRoutes} from './order-history/ts/routes/order-history.routes'

export const routes:Routes = [
  ...BookRoutes,
  ...CartRoutes,
  ...CheckoutRoutes,
  ...OrderOptionRoutes,
  ...LoginRoutes,
  ...ConfirmationRoutes,
  ...AccountCreatorRoutes,
  ...OrderHistoryRoutes
];//Jede Routes-Komponente extends Routes und hat URL-Pfad (path) und eine zugeordnete Komponente (component)
// Der Pfad (path: **) ** kann man für Error 404 verwenden.
//{path: 'bookdetail/:id', component:BookDetailComponent}  Param=id
//  { path: '', redirectTo: '/heroes': '' ist root: wird zu /heroes redirected.
//    data: { title: 'Heroes List' } man kann auch zusätzlichte Infos über das Routing übergeben.
// Wenn im Browser eine URL eingegeben wird, dann wird der entsprechende Inhalt von <router-outlet/> (siehe app.html) aufgerufen
//Start der Anwendung:
//index.html: <bookshop>...Loading</bookshop> 
//bookshop ist der Name von app.component.ts
//app.html: ist das Template von app.component.ts
// Enthät eine PrimeNG: p-menubar und <router-outlet/> für Routing (siehe obenstehende Beschreibung).
export const routing = RouterModule.forRoot(routes, { useHash: true });


