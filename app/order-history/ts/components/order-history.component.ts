import {Component} from '@angular/core';
import {OrderHistory} from '../models/order-history.model';
import {OrderHistoryService} from '../services/order-history.service';

@Component({
  selector: 'orderhistory',
  styleUrls: ['app/css/styles.css'],
  templateUrl: 'app/order-history/templates/orderhistory.html',
})
export class OrderHistoryComponent {
  private _historyList:Array<OrderHistory>;
  constructor(private _orderHistoryService:OrderHistoryService) {
    this._orderHistoryService.getHistoryList()
      .subscribe(res => this._historyList = res); 
  }

  get historyList():Array<OrderHistory> {
    return this._historyList;
  }

  get hasContent():boolean {
    if (this._historyList == undefined) {
      return false;
    }
    return this._historyList.length > 0;
  }
}

