import {OrderHistoryComponent} 
  from "../components/order-history.component";
import {Bouncer} 
  from '../../../login/ts/components/bouncer.component';

export const OrderHistoryRoutes = [
  {path: 'orderhistory', component:OrderHistoryComponent,
       canActivate: [Bouncer]}
];
    
