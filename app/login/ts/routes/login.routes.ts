import {LoginComponent} from "../components/login.component";
import {Routes} from '@angular/router';

export const LoginRoutes:Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'login/:fw', component: LoginComponent}
];
