import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {Router} from "@angular/router";

@Injectable()
export class Bouncer implements CanActivate {

  constructor(private _authService: AuthService, 
              private router:Router) {}
  
  canActivate() {
    if (!this._authService.isLoggedIn()) {
      this.router.navigate(['/login','orderhistory']);
    }
    return this._authService.isLoggedIn();
  }
}
