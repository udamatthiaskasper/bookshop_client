import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {InputTextModule, ButtonModule, MenubarModule, DataGridModule, 
        PanelModule, DataListModule, GrowlModule, DataTableModule, 
        DropdownModule, MessagesModule} from 'primeng/primeng';
import {routing} from './app.routes';
import {BookListComponent} from './book/ts/components/booklist.component';
import {HttpModule, JsonpModule} from '@angular/http';
import {BookListService} from './book/ts/services/booklist.service';
import {BookDetailService} from './book/ts/services/bookdetail.service';
import {BookDetailComponent} from './book/ts/components/bookdetail.component';
import {CartComponent} from './cart/ts/components/cart.component';
import {CheckoutComponent} from './checkout/ts/components/checkout.component';
import {Cart} from './cart/ts/models/cart.model';
import {CheckoutService} from './checkout/ts/services/checkout.service';
import {OrderOptionComponent}  from './order-option/ts/components/order-option.component';
import {LoginComponent} from './login/ts/components/login.component';
import {AuthService} from './login/ts/services/auth.service';
import {Account} from './login/ts/models/account.model';
import {ConfirmationComponent}  from './confirmation/ts/components/confirmation.component';
import {AccountCreatorComponent}  from './accountcreator/ts/components/account-creator.component';
import {AccountCreatorService}  from './accountcreator/ts/services/account-creator.service';
import {OrderHistoryComponent}  from './order-history/ts/components/order-history.component';
import {OrderHistoryService}  from './order-history/ts/services/order-history.service';
import {Bouncer}  from './login/ts/components/bouncer.component';
import {PipesModule}  from './pipes/pipes.module';
import {FilterComponent} from './filter/ts/components/filter.component';
@NgModule({
  imports: [BrowserModule, InputTextModule, ButtonModule, FormsModule, 
            MenubarModule, routing, HttpModule, JsonpModule, 
            DataGridModule, PanelModule, DataListModule, GrowlModule, 
      DataTableModule, DropdownModule, MessagesModule,PipesModule],
  declarations: [AppComponent, LoginComponent, BookListComponent, 
                 BookDetailComponent, CartComponent, CheckoutComponent, 
                 OrderOptionComponent, OrderHistoryComponent,
      ConfirmationComponent, AccountCreatorComponent, FilterComponent],
  bootstrap: [AppComponent],
  providers: [BookListService, BookDetailService, Cart, CheckoutService, 
              AuthService, Account, AccountCreatorService, 
              OrderHistoryService,Bouncer]
})
export class AppModule { }