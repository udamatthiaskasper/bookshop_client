import 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {BookDetail} from '../models/bookdetail.model';

@Injectable()
export class BookDetailService {
//Endpoint Service für ein Book-Objekt
  constructor(private http: Http) { }

  getBookDetail(id:number):Observable<BookDetail> {
    let endpoint_url:string 
       = 'http://localhost:8080/http/bookdetails?id=' + id;
    return this.http.get(
      endpoint_url, {method: 'Get'}).map( (responseData) => {
        return responseData.json();
    })
    .map((bookDetail:BookDetail) => {
      let result:BookDetail;
      if (bookDetail) {
        result = new BookDetail(
          bookDetail.id,
          bookDetail.name,
          bookDetail.description,
          bookDetail.size,
          bookDetail.price,
          bookDetail.image);
        }
        if (result == null) {
          console.log('ERROR: result == null');
      }
      return result;
    });
  }
}


