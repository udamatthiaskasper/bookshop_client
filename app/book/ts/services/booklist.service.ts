import "rxjs/Rx";
import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Book} from '../models/book.model';
//Webservice-Endpoint, Parameter ist der Typ: Vegetarische, Alle etc.
@Injectable()
export class BookListService {
  constructor(private http:Http) { }
  //Webservice, je nach Type-Var wird eine andere Liste geholt
  //JSON-Ergebnis wird in ein Observable<Array<Book>> umgewandelt.
  
  getBookList(bookType:string):Observable<Array<Book>> {//Gibt ein asynchrones Observable zurück, vergleichbar mit Java-Futures-Objekt
    let endpoint_url:string 
       = "http://localhost:8080/http/booklist?bookType=" + bookType;
    return this.http.get(endpoint_url, {method: 'Get'})
    .map( (responseData) => {
      return responseData.json();
    })
    .map((bookList: Array<any>) => {
      let result:Array<Book> = [];
      if (bookList) {
        bookList.forEach((book) => {
          result.push(new Book(book.id,
                                book.name,
                                book.size,
                                book.price,
                                book.image));
          });
        }
        console.log(result.length + " Pizzen erhalten.");
        return result;
      }
    ).catch(this.handleError);
  }

  private handleError(error:Response):Observable<any> {
    return Observable
       .throw(error.json().error || 'Der Server ist nicht erreichbar.');
  }
}