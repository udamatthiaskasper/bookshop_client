import {BookListComponent} from "../components/booklist.component";
import {Routes} from '@angular/router';
import {BookDetailComponent} from "../components/bookdetail.component";

export const BookRoutes:Routes = [
  {path: '', component:BookListComponent},
  {path: 'bookdetail/:id', component:BookDetailComponent}
];


