import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {BookListService} from '../services/booklist.service';
import {Book} from '../models/book.model';
import {FilterModel} from '../../../filter/ts/models/filter.model';
import {Message} from 'primeng/primeng';
//Booklist und Bookdetail werden im Bereich <router-outlet/> von app.html geroutet, Booklist der Default-Root-Wert
//Bookdetail wird mit /bookdetail/id des Objekts als Parameter
//
@Component({
  selector: 'booklist',
  templateUrl: 'app/book/templates/booklist.html',
  styleUrls: ['app/css/styles.css']
})
export class BookListComponent {

  private bookList:Array<Book>;
  model:FilterModel = new FilterModel();
  msgs:Message[] = [];

  constructor(private _bookListService:BookListService, 
              private _router:Router) {
    this.getFilteredSelectionList();
  }

  getFilteredSelectionList() {//subscribe: Holt Obervable von Webservice ab.Service: nach jeder Änderung der DDLB Book-Filter aufgerufen.
    this._bookListService.getBookList(this.model.selectedBookType)
      .subscribe(res => this.bookList = res, 
      error => {
        console.log(error);
        this.msgs.push({severity: 'error',
        summary: 'Ein Fehler ist aufgetreten!', detail: error});
      }
    );
  }

  getBookList(){ return this.bookList; }
  showDetails(book:Book) {
    this._router.navigate(['bookdetail', book.id]);
  }
}
 

