import {Component} from '@angular/core';
import {BookDetail} from '../models/bookdetail.model';
import {BookDetailService} from '../services/bookdetail.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Cart} from '../../../cart/ts/models/cart.model';
import {CartItem} from '../../../cart/ts/models/cartitem.model';

@Component({
  selector: 'bookdetail', 
  templateUrl: 'app/book/templates/bookdetail.html',
  styleUrls: ['app/css/styles.css']
})
export class BookDetailComponent {

  private _bookDetail:BookDetail;

  constructor(private _bookDetailService:BookDetailService,
              private _route:ActivatedRoute,
              private _location:Location,
              private _cart:Cart) {
                let id = this._route.snapshot.params['id'];
                this._bookDetailService.getBookDetail(id).subscribe(
                  bookDetail => {
                    this._bookDetail = bookDetail;
                  },
                  err => console.error(err),
                  () => console.log('done: ' + this._bookDetail.image));
  }

  addToCart(bookDetail:BookDetail) {
    this._cart.addItem(new CartItem(bookDetail.id,
                       bookDetail.price,
                       bookDetail.image,
                       bookDetail.size,
                       bookDetail.name));
  }

  get currentAmount():number {
    let currentAmount = 0;
    for (let i = 0; i < this._cart.getContent().length; i++) {
      if (this._cart.getContent()[i].bookId == this._bookDetail.id) {
        currentAmount = this._cart.getContent()[i].amount;
      }
    }
    return currentAmount;
  }

  historyBack() {
    this._location.back();
  }

  get bookDetail():BookDetail {
    return this._bookDetail;
  }
}
   


