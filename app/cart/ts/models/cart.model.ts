import {CartItem} from './cartitem.model';

export class Cart {
//Cart verwaltet einen Array aus CartItems
  private _content:Array<CartItem>

  constructor() {
    this._content = new  Array<CartItem>();
  }
  
  getContent():Array<CartItem> {
    return this._content;
  }
  
  flush() {//Alle Objekte löschen
    this._content = new   Array<CartItem>();
  }

  get sumtotal():number {
    let sum = 0;
    this._content.forEach(function(item) {
      sum += item.subtotal;
    });
    return sum;
  }

  addItem(item:CartItem) {
    let cartItem = new
    CartItem(item.bookId, item.price, item.image, item.size, item.name);
    let found:boolean = false;
    this._content.forEach(function(item) {
      if (item.bookId == cartItem.bookId) {
        // Das gewählte Objekt war bereits im Warenkorb, 
        // die Anzahl wird um 1 erhöht
        item.increaseAmount();
        found = true;
      }
    });
    if (!found) {
      // Das gewählte Objekt ist noch nicht im Warenkorb 
      // und wird mit Anzahl = 1 hinzugefügt
      this._content.push(cartItem);
    }
  }
  
  decreaseItem(item:CartItem) {
    let cartItem = new CartItem(item.bookId, 
                                item.price, 
                                item.image, 
                                item.size, 
                                item.name);
    for (let i = 0; i < this._content.length; i++) {
      if (this._content[i].bookId == cartItem.bookId) {
        if (this._content[i].decreaseAmount() == 0) {
          this._content.splice(i, 1);
        }
      }
    }
  }

  removeItem(item:CartItem) {
    let cartItem = new CartItem(item.bookId, 
                                item.price, 
                                item.image, 
                                item.size, 
                                item.name);
    for (let i = 0; i < this._content.length; i++) {
      if (this._content[i].bookId == cartItem.bookId) {
        this._content.splice(i, 1);//löscht Elemente aus einen Array: splice(startCounter,Anzahl zu Löschende ab Start)
      }
    }
  }
}


