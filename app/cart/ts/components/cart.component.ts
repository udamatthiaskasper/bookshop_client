import {Component} from '@angular/core';
import {Cart} from '../../../cart/ts/models/cart.model';
import {CartItem} from '../../../cart/ts/models/cartitem.model';
import {Router} from '@angular/router';
import {CheckoutService} from '../../../checkout/ts/services/checkout.service';

@Component({
  selector: 'cart',
  templateUrl: 'app/cart/templates/cart.html',
  styleUrls: ['app/css/styles.css']
})
export class CartComponent {

  constructor(private _router:Router, 
              private _cart:Cart, 
              private _checkoutService:CheckoutService) { }

  checkout() {
  /*  this._checkoutService.checkout()
      .subscribe(res => console.log("checkout: " + res.statusCode));
    this._cart.flush();
    this._router.navigate(['checkout']);*/
  /*  --Kapitel 8 deleted Seite 220 */
  
 //   this._router.navigate(['order-option']);
      if (localStorage.getItem('auth-token')==null){
        this._router.navigate(['order-option']);
      }else{
          this._router.navigate(['confirmation']);
      }
  }
  
  get sumtotal():number {
    return this._cart.sumtotal;
  }

  decreaseItemAmount(item:CartItem) {
    this._cart.decreaseItem(item);
  }

  increaseItemAmount(item:CartItem) {
    this._cart.addItem(item);
  }

  removeItem(item:CartItem) {
    this._cart.removeItem(item);
  }

  get hasContent():boolean {
    return this._cart.getContent().length > 0;
  }
}


