import {CartComponent} from '../components/cart.component';
import {Routes} from '@angular/router';

export const CartRoutes:Routes = [
  {path: 'cart', component: CartComponent}
];//cart ist der URL-Pfad, muss noch in app.routes.ts eingetragen werden.


